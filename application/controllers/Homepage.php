<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {
	public function index(){
		//$this->load->view("forms_view");
		$this->load->model('forms_model');

		$data["fetch_data"] = $this->forms_model->fetch_data();

		$this->load->view("forms_view", $data);

	}

	public function form_validation() {
		$this->load->library("form_validation");
		$this->form_validation->set_rules("first_name", "First Name", 'required');
		$this->form_validation->set_rules("last_name", "Last Name", 'required');
		if($this->form_validation->run()) {
			print_r($this->input->post());
			$this->load->model("forms_model");
			$data = array(
				"first_name" => $this->input->post("first_name"),
				"last_name" => $this->input->post("last_name")
			);
			$this->forms_model->insert_data($data);
		} else {
			$this->index();
		}
	}

	public function inserted() {
		echo "inserted";
	}
}